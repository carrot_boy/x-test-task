<?php

use Phinx\Seed\AbstractSeed;

class UserSeeder extends AbstractSeed
{
    public function run() {
        if ($this->hasTable('users')) {
            $data = $this->getSeedData();
            $users = $this->table('users');
            // truncate
            $this->execute('DELETE FROM users');
            $users->insert($data)
                  ->save();
        }
    }

    /**
     * Generate data to insert in the table
     * @return array
     */
    private function getSeedData() {
        $testUser = 'test-user';
        $userWithExpiredToken = 'user-with-expired-token';

        return [
            [
                'username' => $testUser,
                'name' => 'Тестовый пользователь',
                'folder' => 'test',
            ],
            [
                'username' => $userWithExpiredToken,
                'name' => 'Пользователь с истекшим токеном',
                'folder' => '',
            ],
        ];
    }
}
