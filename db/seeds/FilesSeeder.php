<?php


use Phinx\Seed\AbstractSeed;

class FilesSeeder extends AbstractSeed
{
    public function run() {
        if ($this->hasTable('user_files')) {
            $data = $this->getSeedData();
            $users = $this->table('user_files');
            // truncate
            $this->execute('DELETE FROM user_files');
            $users->insert($data)
                  ->save();
        }
    }

    /**
     * Generate data to insert in the table
     * @return array
     */
    private function getSeedData() {
        return [
            [
                'username' => 'test-user',
                'name' => 'loader.gif',
                'real_name' => 'loader.gif',
                'bytes' => '673',
            ]
        ];
    }
}
