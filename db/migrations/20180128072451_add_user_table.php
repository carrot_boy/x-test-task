<?php

use Phinx\Migration\AbstractMigration;

class AddUserTable extends AbstractMigration
{
    public function up() {
        $users = $this->table('users');
        $users->addColumn('username', 'string', ['limit' => 20])
              ->addColumn('name', 'string', ['limit' => 40])
              ->addColumn('folder', 'string', ['limit' => 40])
              ->addIndex(['username', 'folder'], ['unique' => true])
              ->save();
    }

    public function down() {
        $this->dropTable('users');
    }
}
