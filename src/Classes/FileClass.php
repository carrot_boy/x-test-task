<?php

namespace App\Classes;

use League\Flysystem\Filesystem;
use Slim\Http\UploadedFile;
use App\Models\File;

/**
 * Class FileClass
 */
class FileClass
{
    protected $filesystem;
    protected $file;

    /**
     * FileClass constructor.
     *
     * @param Filesystem $filesystem
     * @param UploadedFile $file
     */
    public function __construct(Filesystem $filesystem, UploadedFile $file) {
        $this->filesystem = $filesystem;
        $this->file = $file;
    }

    /**
     * @return string
     */
    public function generateNewFilename() {
        $name = $this->file->getClientFilename();
        $baseName = hash('crc32b', $name);
        $extension = pathinfo($name, PATHINFO_EXTENSION);

        return sprintf('%s.%0.8s', $baseName, $extension);
    }

    /**
     * @param string $filename
     *
     * @return bool
     */
    public function isFileRecordExists($filename) {
        return (File::where('name', $filename)->count() > 0);
    }

    /**
     * @param string $filename
     *
     * @return bool
     */
    public function isFileExists($filename) {
        return $this->filesystem->has($filename);
    }

    /**
     * @param string $path
     *
     * @return bool
     */
    public function saveFile($path) {
        $content = $this->file->getStream();

        return $this->filesystem->write($path, $content);
    }

    /**
     * @param string $path
     * @param string $oldPath
     *
     * @return bool
     */
    public function replaceFile($path, $oldPath) {
        $this->filesystem->delete($oldPath);
        $content = $this->file->getStream();

        return $this->filesystem->write($path, $content);
    }

    /**
     * @param string $username
     * @param string $filename
     *
     * @return mixed
     */
    public function createFileRecord($username, $filename) {
        return File::create([
            'username' => $username,
            'name' => $filename,
            'real_name' => $this->file->getClientFilename(),
            'bytes' => $this->file->getSize(),
        ]);
    }

    /**
     * @param integer $id
     * @param string  $filename
     *
     * @return mixed
     */
    public function updateFileRecord($id, $filename) {
        $now = new \DateTime();
        return File::where('id', $id)
            ->update([
                'name' => $filename,
                'real_name' => $this->file->getClientFilename(),
                'bytes' => $this->file->getSize(),
                'updated_at' => $now->format('Y-m-d H:i:s'),
            ]);
    }
}
