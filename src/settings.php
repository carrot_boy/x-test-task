<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Database settings
        'db' => [
            'driver'   => 'sqlite',
            'database' => __DIR__.'/../db/store.db',
            'prefix'   => '',
        ],

        // Filesystem settings
        'fs' => __DIR__.'/../user_files/',

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            // 'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
            'path' => 'php://stdout',
            'level' => \Monolog\Logger::DEBUG,
        ],
    ],
];
