<?php
// DIC configuration
$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

// database connection
$container['db'] = function ($c) {
    $capsule = new \Illuminate\Database\Capsule\Manager();
    $capsule->addConnection($c->get('settings')['db']);
    $capsule->setAsGlobal();
    $capsule->bootEloquent();

    return $capsule;
};

// filesystem
$container['fs'] = function ($c) {
    $basePath = $c->get('settings')['fs'];
    $adapter = new \League\Flysystem\Adapter\Local($basePath);

    return new \League\Flysystem\Filesystem($adapter);
};

// middleware
$container['apiMiddleware'] = function ($c) {
    return new App\Middleware\AuthMiddleware($c);
};

$container['fileMiddleware'] = function ($c) {
    return new App\Middleware\FileMiddleware($c);
};

$container['createMiddleware'] = function ($c) {
    return new App\Middleware\CreateMiddleware($c);
};

$container['uploadMiddleware'] = function ($c) {
    return new App\Middleware\UploadMiddleware($c);
};

// controllers
$container['apiController'] = function ($c) {
    return new App\Controllers\ApiController($c);
};
