<?php

namespace App\Middleware;

use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\UploadedFile;
use App\Classes\FileClass;

class CreateMiddleware
{
    /** @var ContainerInterface */
    protected $c = null;

    /**
     * AuthMiddleware constructor.
     *
     * @param ContainerInterface $c
     */
    public function __construct($c) {
        $this->c = $c;
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param          $next
     *
     * @return string
     */
    public function __invoke($request, $response, $next) {
        $uploadedFiles = $request->getUploadedFiles();
        /** @var UploadedFile $file */
        $file = $uploadedFiles['file'];
        $helper = new FileClass($this->c->fs, $file);
        $filename = $helper->generateNewFilename();

        if ($helper->isFileRecordExists($filename)) {
            return $response->withStatus(409)
                ->withJson([
                    'code' => 409,
                    'message' => 'File already exists',
                ]);
        }

        if ($helper->isFileExists($filename)) {
            return $response->withStatus(409)
                ->withJson([
                    'code' => 409,
                    'message' => "File already exists",
                ]);
        };

        return $next($request, $response);
    }

}