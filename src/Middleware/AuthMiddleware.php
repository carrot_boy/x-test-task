<?php

namespace App\Middleware;

use Illuminate\Support\Collection;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class AuthMiddleware
{
    /** @var ContainerInterface */
    protected $c = null;

    /**
     * AuthMiddleware constructor.
     *
     * @param ContainerInterface $c
     */
    public function __construct($c) {
        $this->c = $c;
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param          $next
     *
     * @return string
     */
    public function __invoke($request, $response, $next) {
        $token = $request->getQueryParam('token');
        if (empty($token)) {
            return $this->responseError($response, 'Empty token');
        }

        $record = $this->getTokenRecord($token);
        if ($record->isEmpty()) {
            return $this->responseError($response, 'Invalid token');
        }

        $record = $record->first();
        if ($this->isTokenExpired($record)) {
            return $this->responseError($response, 'Expired token');
        }

        // pass the user info to the next
        $user = $this->getUserRecord($record->username);
        $newRequest = $request->withAttribute('user', $user);

        return $next($newRequest, $response);
    }

    /**
     * @param Response $response
     * @param          $message
     *
     * @return string
     */
    private function responseError(Response $response, $message) {
        return $response->withStatus(401)
            ->withJson([
                'code' => 401,
                'message' => $message,
            ]);
    }

    /**
     * @param Collection $token
     *
     * @return bool
     */
    private function isTokenExpired($token) {
        $now = new \DateTime();
        $dExpired = \DateTime::createFromFormat('Y-d-m H:i:s', $token->expired_at);

        return ($now > $dExpired);
    }

    /**
     * @param $token
     *
     * @return Collection
     */
    private function getTokenRecord($token) {
        return $this->c->db->table('user_tokens')
            ->where('hash', '=', $token)
            ->get();
    }

    /**
     * @param $username
     *
     * @return Collection
     */
    private function getUserRecord($username) {
        return $this->c->db->table('users')
            ->where('username', '=', $username)
            ->first();
    }
}
