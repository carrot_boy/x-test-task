<?php

namespace Tests\Functional;

class AuthMiddleWareTest extends BaseTestCase
{
    protected $baseUrl = '/api/v1/list';
    protected $expiredToken = 'XPR-USR';
    protected $correctToken = 'TST-USR';

    /**/
    public function testApiWithoutToken() {
        $response = $this->runApp('GET', $this->baseUrl);
        $this->assertEquals(401, $response->getStatusCode());

        $json = (string) $response->getBody();
        $this->assertJson($json);

        // check structure of error and error message
        $arr = json_decode($json, true);
        $this->assertArrayHasKey('message', $arr);
        $this->assertEquals('Empty token', $arr['message']);
    }

    /**/
    public function testApiWithWrongToken() {
        $url = $this->baseUrl . '?token=wrong-token';
        $response = $this->runApp('GET', $url);
        $this->assertEquals(401, $response->getStatusCode());

        $json = (string) $response->getBody();
        $this->assertJson($json);

        // check structure of error and error message
        $arr = json_decode($json, true);
        $this->assertArrayHasKey('message', $arr);
        $this->assertEquals('Invalid token', $arr['message']);
    }

    /**/
    public function testApiWithExpiredToken() {
        $url = $this->baseUrl . '?token=' . $this->expiredToken;
        $response = $this->runApp('GET', $url);
        $this->assertEquals(401, $response->getStatusCode());

        $json = (string) $response->getBody();
        $this->assertJson($json);

        // check structure of error and error message
        $arr = json_decode($json, true);
        $this->assertArrayHasKey('message', $arr);
        $this->assertEquals('Expired token', $arr['message']);
    }

    /**/
    public function testApiWithCorrectToken() {
        $url = $this->baseUrl . '?token=' . $this->correctToken;
        $response = $this->runApp('GET', $url);
        $this->assertEquals(200, $response->getStatusCode());

        $json = (string) $response->getBody();
        $this->assertJson($json);
    }
}
