# Xsolla test task

Test task for Xsolla company ([source](https://docs.google.com/document/d/1zPe7OoCaPOZw5rd-sgh3jpCZ3_U_sdshe7hPqtKxb6Y/edit#)).

Solution based on [slimphp/Slim-Skeleton](https://github.com/slimphp/Slim-Skeleton)

## Dependencies

- PHP v7.1
- PHPUnit
- Composer
- Docker-compose

## How to run

1. Clone this repository: `git clone https://gitlab.com/TheAnarchist/x-test-task.git`
2. `cd x-test-task`
3. **Important!** Give permissions(recursive) to write for folders _db_, _user_files_: `chmod -R a+w {db,user_files}` as root
4. Install dependencies: `composer install`
5. Run functional tests: `composer run test`
6. Start server:
    - Built-in PHP server: `composer run start:dev`
    - Docker-compose: `composer run start`

## How to use

[Documentation](docs/readme.md)

Now 2 users exist:

- User with expired token (for tests)
- Test user with token 'TST-USR'

Now you can use test user to try this API.

## How to create a new user

You should create a new record in the `users` table. There is no other ways to create users for now(added to _todos_).

## How to get token

You should create a new record in the `user_tokens` table for new user. But you can't create 2 tokens for one user. There is no other ways to create tokens for now(added to _todos_).

## TODO

[Russian](TODO_RU.md)