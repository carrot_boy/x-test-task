# Show list of user's files

**URL** : `/api/v1/list/?token=:user_token`

**Method** : `GET`

## Success Responses

**Code** : `200 OK`

**Content** :

```json
[
    {
        "id": 123,
        "name": "test_file.txt"
    },
    {
        "id": 234,
        "permission": "hello.world"
    }
]
```

[Back](readme.md)