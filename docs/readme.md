# Documentation

## Endpoints that require Authentication

Closed endpoints require a valid Token to be included as query parameter to the request.

Every request to require token for access to API. Endpoints for viewing and manipulating the files.

* [Create file from request content](create.md) : `POST /api/v1/create/`
* [Update file from request content](update.md) : `POST /api/v1/update/:id/`
* [Get file content](get.md) : `GET /api/v1/get/:id/`
* [Show file metadata](meta.md) : `GET /api/v1/meta/:id/`
* [Show list of user's files](list.md) : `GET /api/v1/list/`

## Errors

When errors occurr the server responds with appropriate HTTP-code and payload. All payload have the same format.

For example:

```json
{
    "code": 404,
    "message": "File not found"
}
```

[Back](../README.md)